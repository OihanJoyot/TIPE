import matplotlib.pyplot as plt
import numpy

"""

"""
nnodes = [0, 8, 16, 32, 64, 128, 256, 512, 1024]
times = [0, 0.16549324989318848, 0.4445435206095378, 0.9208056131998698, 1.8371704419453938, 3.361276865005493, 6.331358353296916, 13.041333357493082, 25.888836701711018]

p1 = numpy.polyfit(nnodes, times, 1)
print(p1)

droite = lambda n: n*p1[0]+p1[1]
fig = plt.figure()
# fig.subplots_adjust(top=0.8)
ax1 = fig.add_subplot(111)
ax1.set_xlabel("nombre d'agent")
ax1.set_ylabel('temps (s)')
ax1.set_title('t=(%2.2f*n+%2.2f)*1e-2' % (p1[0]*100, p1[1]*100))

ax1.plot(nnodes, times, 'o')
ax1.plot([0, 1024], [droite(0), droite(1024)], '-')

# plt.show()
plt.savefig('convglobal.pdf')
"""
import time

nrun = 3
ltime = []
lnbrajent = []
ltimemoy = []
for p in range(3, 11):
    nombre_agents = 2**p
    lnbrajent.append(nombre_agents)
    for i in range(nrun):
        b = time.time()
        S = Systeme(nombre_agents, dt=0.01, pilier = False).euler(tf=1)
        e = time.time()
        ltime.append(e-b)

    print(nombre_agents)
    print(ltime)
    tmoy = sum(ltime)/nrun
    print(tmoy)
    ltimemoy.append(tmoy)

print(lnbrajent)
print(ltimemoy)

100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 669.97it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 615.90it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 567.06it/s]
8
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926]
0.16549324989318848
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 386.50it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 360.35it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 336.97it/s]
16
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914]
0.4445435206095378
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 225.17it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 221.97it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 188.64it/s]
32
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547]
0.9208056131998698
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 119.83it/s]
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 110.47it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:01<00:00, 99.57it/s]
64
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547, 0.8364167213439941, 0.9066879749298096, 1.0059897899627686]
1.8371704419453938
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:01<00:00, 65.23it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:01<00:00, 65.34it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:01<00:00, 66.52it/s]
128
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547, 0.8364167213439941, 0.9066879749298096, 1.0059897899627686, 1.5351464748382568, 1.5322175025939941, 1.5049552917480469]
3.361276865005493
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:02<00:00, 34.25it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:02<00:00, 34.07it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:03<00:00, 32.80it/s]
256
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547, 0.8364167213439941, 0.9066879749298096, 1.0059897899627686, 1.5351464748382568, 1.5322175025939941, 1.5049552917480469, 2.9215147495269775, 2.937163829803467, 3.0515658855438232]
6.331358353296916
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:07<00:00, 13.66it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:06<00:00, 15.27it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:06<00:00, 16.00it/s]
512
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547, 0.8364167213439941, 0.9066879749298096, 1.0059897899627686, 1.5351464748382568, 1.5322175025939941, 1.5049552917480469, 2.9215147495269775, 2.937163829803467, 3.0515658855438232, 7.324160575866699, 6.553430080413818, 6.252334356307983]
13.041333357493082
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:13<00:00,  7.62it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:13<00:00,  7.54it/s]
100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 100/100 [00:12<00:00,  8.24it/s]
1024
[0.15494132041931152, 0.16384100914001465, 0.17769742012023926, 0.26032090187072754, 0.27883481979370117, 0.29799509048461914, 0.44548463821411133, 0.4518857002258301, 0.5314159393310547, 0.8364167213439941, 0.9066879749298096, 1.0059897899627686, 1.5351464748382568, 1.5322175025939941, 1.5049552917480469, 2.9215147495269775, 2.937163829803467, 3.0515658855438232, 7.324160575866699, 6.553430080413818, 6.252334356307983, 13.129560947418213, 13.272944450378418, 12.140004634857178]
25.888836701711018
[8, 16, 32, 64, 128, 256, 512, 1024]
[0.16549324989318848, 0.4445435206095378, 0.9208056131998698, 1.8371704419453938, 3.361276865005493, 6.331358353296916, 13.041333357493082, 25.888836701711018]
"""
