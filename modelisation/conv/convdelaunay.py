
from scipy.spatial import Delaunay
import numpy as np

# points = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
# print(points)
# tri = Delaunay(points)

# print(tri.simplices)

# print(tri.vertex_to_simplex)
# print(tri.vertex_neighbor_vertices)

# for i in range(4):
#     print(points[i,:])
#     print(tri.find_simplex(points[i, :]))

"""
import numpy.random as rd
import time
import math

nnodes = []
nnodes_nlnn = []
times = []
for p in range(2, 20):
    N = 2**p
    nnodes.append(N)
    nnodes_nlnn.append(N*math.log(N))

    C = np.zeros((N, 2))
    for i in [0, 1]:
        C[:, i] = rd.uniform(0.5, 2.0-0.5, N)

    b = time.time()
    # for i in range(1):
    Delaunay(C)
    e = time.time()

    print(N)
    print(e-b)
    print((e-b)/N)
    print((e-b)/(N*math.log(N)))
    times.append(e-b)

print(nnodes)
print(nnodes_nlnn)
print(times)
"""

nnodes = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288]
nnodes_nlnn = [5.545177444479562, 16.635532333438686, 44.3614195558365, 110.90354888959125, 266.168517335019, 621.059873781711, 1419.565425786768, 3194.022208020228, 7097.82712893384, 15615.219683654448, 34069.57021888243, 73817.40214091193, 158991.327688118, 340695.7021888243, 726817.4980028252, 1544487.1832560035, 3270678.7410127134, 6904766.231026839]
times = [0.00025200843811035156, 0.00014162063598632812, 0.00016379356384277344, 0.0002148151397705078, 0.00038623809814453125, 0.0006034374237060547, 0.0013241767883300781, 0.0022857189178466797, 0.007828474044799805, 0.02478480339050293, 0.03585529327392578, 0.08508157730102539, 0.11556601524353027, 0.24808239936828613, 0.46098995208740234, 1.1399872303009033, 2.224702835083008, 4.822653293609619]

import matplotlib.pyplot as plt
import numpy

lnodes = nnodes_nlnn

p1 = numpy.polyfit(lnodes, times, 1)
print(p1)

droite = lambda n: n*p1[0]+p1[1]
fig = plt.figure()
# fig.subplots_adjust(top=0.8)
ax1 = fig.add_subplot(111)
ax1.set_xlabel("n*ln(n) agents")
ax1.set_ylabel('temps (s)')
ax1.set_title('Delaunay t=(%2.2e*n*ln(n)+%2.2e)' % (p1[0], p1[1]))

ax1.plot(lnodes, times, 'o')
ax1.plot([0, 6904766], [droite(0), droite(6904766)], '-')

# plt.show()
plt.savefig('convdelaunay.pdf')