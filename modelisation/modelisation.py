import numpy as np
import numpy.random as rd
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from scipy.spatial import Delaunay

vec = lambda *c: np.array(c)
def mod(v): return (v@v)**0.5
def perp(v): return vec(-v[1], v[0])
def npm(f, m): return np.apply_along_axis(f, 1, m)
def mods(A): return ((np.sum(A**2, axis=1))**(0.5))


class Systeme:
    def __init__(self, nombre_agents=10, dimensions=vec(20, 20),
    largeur_porte=4, dt=0.01, pilier = True, random_masse=False):
        """
        Créer un objet système \n
        nombre_agents (int) -> nombre d'agents entrant dans la simulation \n
        dimensions (np.array) -> vecteur x y où x et y sont les dimensions de la salle \n
        largeur_porte (float) -> largeur de la porte de sortie, 
        celle-ci est située au milieu du mur gauche \n
        dt (float) -> intervalle de temps de discrétisation \n
        random_masse (bool) -> randomizer la masse des agents \n

        -- Complexité en O(n*lnn) --
        """

        
        self.lp = largeur_porte
        self.nombre_agents = nombre_agents

        self.dimensions = dimensions
        self.lx = dimensions[0]
        self.ly = dimensions[1]

        #  Matrice contenant les vecteurs [x, y, vx, vy] des agents
        self.CV = np.zeros((self.nombre_agents, 4))
        # Ici, le slicing par référence de python permet de manipuler des matrices
        # de position et de vitesse indépendamment
        self.C = self.CV[:, 0:2]
        self.V = self.CV[:, 2:4]

        self.A = np.zeros((self.nombre_agents, 2))
        if random_masse:
            self.M = rd.normal(73, 10, (self.nombre_agents)).reshape(-1, 1)
        else:
            self.M = np.full((self.nombre_agents), 73.).reshape(-1, 1)
        self._randomize_positions()
        self.triangulation = Delaunay(self.C) # O(n*lnn)
        self.dt = dt
        self.v0 = 0.8
        self.pilier = pilier

        # Forces de répulsions décrites par Helbing
        A = 2e3 ; B = 8e-2 ; k1 = 1.2e5
        self.f1 = np.vectorize(lambda d: (A*np.exp((1-d)/B)
            + (k1*(1-d) if d < 1 else 0)))
        k2 = 0  # On ne met pas la force de glissement (ne marche pas)
        self.f2 = np.vectorize(lambda d: ((k2*(1-d) if d < 1 else 0)))

        A = 2e3 ; B = 8e-2 ; k = 1.2e5  # pas encore de forces de glissements non plus
        self.f3 = (lambda d: (A*np.exp(d/B)+(k*d if d > 0 else 0)))
    
    def _randomize_positions(self):
        """
        Positionne aléatoirement les agents dans une pièce \n

        -- Complexité en O(n) --
        """
        for i in [0, 1]: self.C[:, i] = rd.uniform(
            0.5, self.dimensions[i]-0.5, self.nombre_agents)

    def plus_proches_voisins(self, i):
        """
        Trouve les voisins à partir des coordonnées d'un agent \n
        i (int) -> identifiant de l'agent \n
            ---> renvoie le vecteur d'identifiants des voisins \n

        -- Complexité en O(1) --
        """
        ibegin = self.triangulation.vertex_neighbor_vertices[0][i]
        iend = self.triangulation.vertex_neighbor_vertices[0][i+1]
        ivoisin = self.triangulation.vertex_neighbor_vertices[1][ibegin:iend]
        return ivoisin

    def force_attractive(self, cv_agent):
        """
        Calcule la force attractive d'un agent \n
        cv_agent (nd.array) -> vecteur [x, y, vx, vy] de l'agent \n
            ---> renvoie le vecteur de force attractive de l'agent \n

        -- Complexité en O(1) --
        """
        c_agent = cv_agent[0:2] # O(1)
        v_agent = cv_agent[2:4] # O(1)
        tau = self.dt
        direction = vec(0., self.ly/2) - c_agent
        norme = mod(direction)
        if norme < self.lp/2-0.5 or c_agent[0] < 0:
            direction = vec(-100000., self.ly/2)
            norme = mod(direction)
        
        if self.pilier and c_agent[0] > 3:
            if c_agent[1] > self.ly/2 : direction = vec(3, 3*self.ly/4) - c_agent
            else : direction = vec(3, 1*self.ly/4) - c_agent

        norme = mod(direction)
        direction /= norme

        return 73/tau*(self.v0*direction-v_agent) # Force décrite par Helbing 


    def force_pilier(self, cv_agent):
        """
        Calcule la force répulsive d'un agent \n
        i (int) -> identifiant de l'agent \n
            ---> renvoie le vecteur de force répulsive de l'agent \n

        -- Complexité en O(1) ici mais en vérité O(n) théorique --
        """
        c_agent = cv_agent[0:2] # O(1)
        v_agent = cv_agent[2:4] # O(1)

        dr = c_agent-vec(3.5, self.ly/2)
        _mod = mod(dr)
        dn = dr/_mod       
        return self.f1(_mod-1)*dn # + (self.f2(A)@(np.sum(dv*dt, axis=1).reshape(-1,1)*dt))

    def force_répulsive(self, i):
        """
        Calcule la force repulsive d'un agent \n
        i (int) -> identifiant de l'agent \n
            ---> renvoie le vecteur de force répulsive de l'agent \n

        -- Complexité en O(1) ici mais en vérité O(n) théorique --
        """
        c_agent = self.C[i[0]] # O(1)
        v_agent = self.V[i[0]] # O(1)
        ppv = self.plus_proches_voisins(i[0]) # O(1)

        dr = c_agent-self.C[ppv] # O(n) théorique mais en pratique O(1)
        _mods = mods(dr)
        dn = dr/_mods.reshape(-1, 1)
        dv = self.V[ppv]-v_agent
        dt = np.zeros(dn.shape)
        dt[:, 0], dt[:, 1] = -dn[:, 1], dn[:, 0]
        
        return self.f1(_mods)@dn # + (self.f2(A)@(np.sum(dv*dt, axis=1).reshape(-1,1)*dt))

    def force_mur(self, cv_agent):
        """
        Calcule la force répulsive d'un agent face au mur\n
        cv_agent (nd.array) -> vecteur [x, y, vx, vy] de l'agent \n
            ---> renvoie le vecteur de force repulsive de l'agent \n

        -- Complexité en O(1) --
        """
        c_agent = cv_agent[0:2] # O(1)
        v_agent = cv_agent[2:4] # O(1)
        def signe(x): return x/abs(x)
        def dist(x): return 0.5-abs(x)

        dist_gauche, dist_bas = (c_agent[0]), (c_agent[1])
        dist_droite, dist_haut = (c_agent[0]-self.lx), (c_agent[1]-self.ly)

        if c_agent[0] <= 0:
            dist_bas = (c_agent[1]-(self.ly/2-self.lp/2))
            dist_haut = (c_agent[1]-(self.ly/2+self.lp/2))

        res = vec(0., 0.)
        res += self.f3(dist(dist_bas))*vec(0., 1.)*signe(dist_bas)
        res += self.f3(dist(dist_droite))*vec(1., 0.)*signe(dist_droite)
        res += self.f3(dist(dist_gauche))*vec(1., 0.) * \
            signe(dist_gauche) if abs(
                c_agent[1]-self.ly/2)+0.5 >= self.lp/2 else 0
        res += self.f3(dist(dist_haut))*vec(0., 1.)*signe(dist_haut)
        return res

    def résultante(self):
        """
        Calcule la résultante des forces pour chaque agent\n
            ---> renvoie la matrice des vecteurs de force résultants pour tous les agents \n

        -- Complexité en O(n) --
        """
        res = np.zeros((self.nombre_agents, 2)) # O(n)
        res += (npm(self.force_attractive, self.CV)) # n*O(1)
        res += (npm(self.force_répulsive, np.arange(self.nombre_agents, )[:, np.newaxis])) # n*O(1)
        res += (npm(self.force_mur, self.CV)) # n*O(1)
        if self.pilier : res += (npm(self.force_pilier, self.CV)) # n*O(1)
        return res

    def vitesse_c(self, v, vlim=4):
        """
        Limite le module d'un vecteur vitesse \n
        v (np.array) -> vecteur vitesse \n
            ---> renvoie le vecteur de même direction, de même sens et de modume max(mod v, vlim) \n

        -- Complexité en O(1) --
        """ 
        norme = mod(v)
        return v if norme < vlim else vlim/norme*v

    def verlet(self, tf=20, correction=False):
        """
        Discrétisation suivant la méthode de Verlet \n
        tf (float) -> temps de simulation
        correction (bool) -> corriger la vitesse
            ---> Renvoie une liste d'états instantannés (self.CV, self.triangulation) \n

        -- Complexité en O(dt/tf*n*lnn) --
        """ 
        tM = [(self.CV, self.triangulation)] # O(1)
        dt = self.dt # O(1)

        for t in tqdm(np.arange(0, tf, dt)):

            if correction:
                _V = npm(self.vitesse_c, self.V + dt/2 *
                         (self.M**-1)*self.résultante()) # n*O(1) + O(n)
            else:
                _V = self.V + dt/2*(self.M**-1)*self.résultante() # O(n)

            self.C[:, :] = self.C + dt*_V #O(n)
            self.triangulation = Delaunay(self.C, incremental=True) # O(n*lnn)

            if correction:
                self.V[:, :] = npm(self.vitesse_c, _V + dt /
                                   2*(self.M**-1)*self.résultante()) # n*O(1) + O(n)
            else:
                self.V[:, :] = _V + dt/2*(self.M**-1)*self.résultante() # O(n)

            tM.append((np.copy(self.CV), self.triangulation)) # O(n)

        return tM

    def euler(self, tf=20, correction=True):
        """
        Discrétisation suivant la méthode de Euler semi-implicite \n
        tf (float) -> temps de simulation
        correction (bool) -> corriger la vitesse
            ---> Renvoie une liste d'états instantannés (self.CV, self.triangulation) \n

        -- Complexité en O(dt/tf*n*lnn) --
        """ 
        tM = [(self.CV, self.triangulation)] # O(1)
        dt = self.dt # O(1)

        for t in tqdm(np.arange(0, tf, dt)):

            self.C[:, :] = self.C + dt*self.V # O(n)
            self.triangulation = Delaunay(self.C) # O(n*lnn)

            if correction:
                self.V[:, :] = npm(self.vitesse_c, self.V +
                                   dt*(self.M**-1)*self.résultante()) # n*O(1) + O(n)
            else:
                self.V[:, :] = self.V + dt*(self.M**-1)*self.résultante() # O(n)

            # if (np.argwhere(self.C[:, 0]>0).shape[0]) == 0 : print (t); break
            tM.append((np.copy(self.CV), self.triangulation)) # O(n)

        return tM

    def animation_live(self, tf=40):
        """
        Affiche une simulation \n 
        tf (float) -> durée de la simulation
        """
        t = self.euler(tf)
        dt = self.dt
        fig, axes = plt.subplots()
        plt.xlim(-5, self.lx)
        plt.ylim(0, self.ly)
        axes.set_aspect(1)
        def circle(x, y): return (plt.Circle((x, y), 0.5))
        cst = 1
        def vect(x, y, dx, dy): return plt.Arrow(x, y, cst*dx, cst*dy)
        lc = [circle(x, y) for [x, y, _, _] in t[0][0]]
        la = [axes.add_artist(vect(x, y, vx, vy))
              for [x, y, vx, vy] in t[0][0]]

        time_text = axes.text(.5, .5, "")

        if self.pilier : axes.add_artist(plt.Circle((3.5, self.ly/2), 1.5, fill = False))
        axes.add_artist(plt.Rectangle((0, 0), self.lx, self.ly, fill=False))
        axes.add_artist(plt.Rectangle(
            (0, self.ly/2-self.lp/2), -30, self.lp, fill=False))
        for c in lc:
            axes.add_artist(c)

        def update(i):
            time_text.set_text(f"t = {round(i*dt, 2)}s")
            axes.lines = []
            for j, [x, y, _, _] in enumerate(t[i][0]):
                lc[j].center = x, y
            for j, [x, y, vx, vy] in enumerate(t[i][0]):
                la[j].remove()
                la[j] = axes.add_artist(vect(x, y, vx, vy))
            a = (plt.triplot(t[i][0][:, 0], t[i]
                 [0][:, 1], t[i][1].simplices)[0])
            return lc + la + [a] + [time_text]

        def progress(i, t): print(f"{i}/{t}")

        ani = FuncAnimation(fig, update, interval=dt*1e3, frames=int(tf/dt),
                            blit=True, save_count=int(tf/dt), repeat=True)
        # ani.save("test.mp4", dpi=300, progress_callback = progress)
        # print("fini")
        plt.show()

    def animation_pdf(self, tf=40, fps=12, folder = "anim_pdfs"):
        """
        Enregistre des images pdfs dans un dossier anim_pdfs (à créer) pour constituer une simulation \n 
        tf (float) -> durée de la simulation
        fps (int) -> nombre d'images par seconde désiré
        """
        t = self.euler(tf)
        dt = self.dt

        def circle(c): return (plt.Circle(c, 0.5))
        cst = 1
        def vect(c): return plt.Arrow(*(cst*c))

        def frame(i, nom):

            fig, ax = plt.subplots()
            plt.xlim(-5, self.lx)
            plt.ylim(0, self.ly)
            ax.set_aspect(1)

            lc = npm(circle, t[i][0][:, 0:2])
            la = npm(vect, t[i][0])

            if self.pilier : ax.add_artist(plt.Circle((3.5, self.ly/2), 1.5, fill = False))
            ax.add_artist(plt.Rectangle((0, 0), self.lx, self.ly, fill=False))
            ax.add_artist(plt.Rectangle(
                (0, self.ly/2-self.lp/2), -30, self.lp, fill=False))

            f = np.vectorize(ax.add_artist)
            f(lc)
            f(la)
            (plt.triplot(t[i][0][:, 0], t[i][0][:, 1],
             t[i][1].simplices, color="black")[0])
            plt.savefig(f"{folder}/{nom}.pdf")
            plt.close()

        _frames = int(tf/dt)
        _fps = 1/self.dt
        n = 1
        for i in tqdm(range(0, _frames, int(_fps/fps))):
            frame(i, n)
            n += 1