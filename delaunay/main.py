import random as rd
import ctypes as ct
import numpy as np
import os
import matplotlib.pyplot as plt

def delaunay(_points, _random=True, n=0):
    """ Entrée : Une liste de points
        [x0, y0, x1, y1, ...]\n
        Sortie : Une matrice numpy de segments qui forment la triangulation
        [[x0, y0], [x1, y1] ...]
    """

    points = np.array([rd.uniform(0,10) for _ in [0]*n]) if _random else _points
    n = len(points)

    _lib = ct.cdll.LoadLibrary("./delaulib.so")
    _lib.delaunay_triangulation.restype = ct.POINTER(ct.c_double)

    
    r =  _lib.delaunay_triangulation((ct.c_double * n)(*points), n)
    length = ct.c_int.in_dll(_lib, "length").value

    nr = np.array(r[:length]).reshape(-1, 4)
    
    # fermeture de la bibliothèque (trouvé sur internet)

    def isLoaded(lib):
        libp = os.path.abspath(lib)
        ret = os.system("lsof -p %d | grep %s > /dev/null" % (os.getpid(), libp))
        return (ret == 0)

    def dlclose(handle):
        libdl = ct.CDLL("libdl.so")
        libdl.dlclose(handle)

    mydll = ct.CDLL('./delaulib.so')
    handle = mydll._handle
    del mydll
    while isLoaded('./delaulib.so'):
        dlclose(handle)
    del points, _lib, handle

    return nr

def draw(_points, _random=True, n=0):
    for s in delaunay(_points, _random, n):
        [x0, y0, x1, y1] = s
        plt.plot([x0, x1], [y0, y1])
    ax = plt.gca()
    ax.set_aspect(1)
    plt.show()

def to_dict(_points, _random=True, n=0):
    return delaunay(_points, _random).reshape(-1,8)
