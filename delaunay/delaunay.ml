type _point = {x: float; y: float} ;;
(*On définit un type _point qui permet d'enregistrer les coordonnées du plan*)
type point = Point of _point | Not_def ;;

(*Le type point permet donc d'exploiter ces coordonnées tout en traitant le cas des points non définis*)

let x p =
(*  Précondition  : un élément p de type point 
    Postcondition : la coordonnée x du point p 
*)
    match p with
    | Not_def -> failwith "Not defined"
    | Point (_p) -> _p.x ;;

let y p =
(*  Précondition  : un élément p de type point
    Postcondition : la coordonnée y du point p
*)
    match p with
    | Not_def -> failwith "Not defined"
    | Point (_p) -> _p.y ;;

let mkp x y = 
(*  Précondition  : deux éléments x de type float, et y de type float
    Postcondition : un élément de type point de coordonnées x, y
*)
    Point({x=x ; y=y})

;;
                              
let random_point max_x max_y =
(*  Précondition  : deux éléments de type flottant qui sont les coordonnées maximales que la fonction peut générer
    Postcondition : un élément de type point de coordonnées aléatoires. 
*)
    mkp
    (Random.float max_x)
    (Random.float max_y);;
                          
let compare_point p1 p2 key = 
(*  Précondition  : deux éléments p1 et p2 de types points
    Postcondition : un entier 1, -1 ou 0 (comparaison)
*)
    let ab =
        match key with
        | "none" ->
            let a = x p1, y p1 in
            let b = x p2, y p2 in
            a, b
        | "inv" ->
            let a = y p1, x p1 in
            let b = y p2, x p2 in
            a, b
        | _ -> failwith "clef non valide" in

    let a, b = ab in
    if a > b then  1 else
    if a < b then -1 else 0;;

let mk_array_of_points size max_x max_y =
(*  Précondition  : 
        Size : de type int 
        max_x : de type float
        max_y : de type float
    Postcondition : un tableau d'éléments de type point triés selon la fonction compare_point
*)
    Array.init size (fun i -> random_point max_x max_y);;

(* ----------------------------------------------- *)
                                                     
type triangle = {p1: point; p2: point; p3: point};;
(*On définit un type triangle formé de trois éléments de type point*)
                                                  
let mkt a b c = 
(*  Précondition  : trois éléments a, b et c de type point
    Postcondition : un élément de type triangle formé par ces trois points
*)
    {p1=a; p2=b; p3=c};;

                                  
type triangle_set = triangle list;;
(*On définit un type triangle_set, qui est un ensemble (concrètement une liste) de triangles.*)

type segment = {a : point ; b : point};;
(*De même on définit un type segment formé de deux éléments de types points*)

let mks a b = 
(*  Précondition  : deux éléments a et b de type point
    Postcondition : un élément de type segment formé de ces deux points
*)
    {a = a ; b = b};;


let compare_segment s1 s2 =
(*  Précondition  : deux éléments s1 et s2 de type segment
    Postcondition : un entier 1, 0 ou  -1 
*)

    if ((x s1.a, y s1.a), (x s1.b, y s1.b)) > ((x s2.a, y s2.a), (x s2.b, y s2.b)) then  1 else
    if ((x s1.a, y s1.a), (x s1.b, y s1.b)) < ((x s2.a, y s2.a), (x s2.b, y s2.b)) then -1 else 0;;

(* ----------------------------------- *)

let is_direct_triangle t = (* Vérifie que le sens trigo est respecté à l'aide du déterminant *)
    let a = t.p1
    and b = t.p2
    and c = t.p3 in
    ((x b -. x a)*.(y c -. y a) -. (y b -. y a)*.(x c -. x a));;

let determinant a b c d e f g h i =
    a*.e*.i
    +. d*.h*.c 
    +. g*.b*.f 
    -. c*.e*.g 
    -. f*.h*.a 
    -. i*.b*.d;;

let incircle d t =
    (* indique si le point est dans le cercle ciconscrit de t *)
    (* t doit être direct *)

    let a = t.p1
    and b = t.p2
    and c = t.p3 in

    let ax = x a and ay = y a in
    let bx = x b and by = y b in
    let cx = x c and cy = y c in
    let dx = x d and dy = y d in

    determinant
    (ax -. dx) (ay -. dy) (((ax**2.) -. (dx**2.)) +. ((ay**2.) -. (dy**2.)))
    (bx -. dx) (by -. dy) (((bx**2.) -. (dx**2.)) +. ((by**2.) -. (dy**2.)))
    (cx -. dx) (cy -. dy) (((cx**2.) -. (dx**2.)) +. ((cy**2.) -. (dy**2.)))

    > 0.;;

let horizontal_spreading points =
    (* Renvoie true s'il y a plus de dispertion verticale qu'horizontale *)
    let n = Array.length points in
    let n_f = float_of_int(n) in
    let sum_x, sum_y, sum_sqx, sum_sqy = ref 0., ref 0., ref 0., ref 0. in
    for i=0 to n-1 do
        let p = points.(i) in
        sum_x := (!sum_x) +. (x p);
        sum_y := (!sum_y) +. (y p);
        sum_sqx := (!sum_sqx) +. ((x p)**2.);
        sum_sqy := (!sum_sqy) +. ((y p)**2.);
    done;
    (!sum_sqx /. n_f) -. (!sum_x /. n_f) ** 2. >
    (!sum_sqy /. n_f) -. (!sum_y /. n_f) ** 2. ;;

(* --------------------------------------------- *)

let mediane points key = let t = Array.copy points in
                     Array.sort (fun a b -> compare_point a b key) t ;
                     t.((Array.length points)/2) 
    (* complexité du tri soit O(nln(n)) *) ;;

(* à partir d'un sujet central 2012 : comment être moins complexe dans le calcul de la médiane *)
type aTernaire = F of point | N of point*aTernaire*aTernaire*aTernaire ;;
let r a = match a with | F(x) -> x | N(x, _,_,_) -> x;;

let pseudo_mediane points key =
    print_string "mediane";
    let mediane3 a0 b0 c0 =
        let abc =
            match key with
            | "none" ->
                let a = x a0, y a0 in
                let b = x b0, y b0 in
                let c = x c0, y c0 in
                a, b, c
            | "inv" ->
                let a = y a0, x a0 in
                let b = y b0, x b0 in
                let c = y c0, x c0 in
                a, b, c
            | _ -> failwith "clef non valide" in
        
        let a, b, c = abc in

        if (a>=b && a<=c) || (a<=b && a>=c) then a0 else
        if (b>=c && b<=a) || (b<=c && b>=a) then b0 else c0 in
    
    let construire_arbre t1 t2 t3 =
        let m = mediane3 (r t1) (r t2) (r t3) in
        N(m, t1, t2, t3) in

    let rec construire t i j =
        (* let r01 = Random.int 2 in *)
        if j = i then F(t.(i)) else
        let p = (j-i+1)/3 in
        let a1 = construire t i (i+p-1) in
        let a2 = construire t (i+p) (i+2*p-1) in
        let a3 = construire t (i+2*p) (i+3*p-1) in
        construire_arbre a1 a2 a3 in

        (* Dans le cas où n est une puissance de 3
        C(n)=3(Cn/3)=9(C(n/9))=...=nC(n/n)=O(n)
        Sinon, cela revient quasiment au même à
        un facteur multiplicatif près *)

    let n = Array.length points in
    if  n < 100 then mediane points key else
    r (construire points 0 ((Array.length points)-1)) ;;

(* -------------------------------------- *)

let array_filter p t =
    let t1 = ref [||] in
    let t2 = ref [||] in
    for i=0 to (Array.length t)-1 do
        if p t.(i) then t1 := Array.concat [(!t1); [|t.(i)|]] else t2 := Array.concat [!t2; [|t.(i)|]]
    done; !t1, !t2 ;;

let array_max _compare t =
    let r = ref t.(0) in
    for i=1 to (Array.length t)-1 do
        if _compare t.(i) !r = 1 then r := t.(i)
    done; !r

let array_min _compare t =
    let r = ref t.(0) in
    for i=1 to (Array.length t)-1 do
        if _compare t.(i) !r = -1 then r := t.(i)
    done; !r

let print_point p =
    print_float (x p) ; print_string " | " ; print_float (y p);;

let print_segment s =
    print_point (s.a) ; print_string " -||- " ; print_point (s.b)

let print_dict2 d =
    Hashtbl.iter (fun k v -> print_char '\n'; print_segment k; print_string " ==================> "; print_point v) d;;

let main points =

    (* si (a, b) est une arête du graphe, si l'on considère
    la liste des arêtes partant de a, si l'arête (a, c) apparait immédiatement
    après l'arête (a, b) en tournant dans le sens trigonométrique, alors
    succ[a, b] = c *)
    
    
    let succ  = Hashtbl.create ((Array.length points)*(Array.length points)) in

    (* idem mais dans le sens horaire *)
    let pred  = Hashtbl.create ((Array.length points)*(Array.length points)) in

    (* prochain point de l'enveloppe convexe en suivant le sens trigo*)
    let first = Hashtbl.create (Array.length points) in
    
    let rec remove_all dict key =
        if not (Hashtbl.mem dict key) then () else
        begin
        Hashtbl.remove dict key;
        remove_all dict key;
        end
        in

    let find = Hashtbl.find in

    let pop dict key = let r = Hashtbl.find dict key in
                    remove_all dict key; r in

    let set dict key value = remove_all dict key; Hashtbl.add dict key value in

    let delete s =
        (* print_string "delete"; *)
        (* Supprime le segment s tout en reconstruisant le réseau de segments
        on met à jour les informations de succ et pred *)
        let s_inv = {a=s.b; b=s.a} in

        let succ_a = pop succ s in 
        let succ_b = pop succ s_inv in
        let pred_a = pop pred s in
        let pred_b = pop pred s_inv in

        set succ {a=s.a; b=pred_a} succ_a;
        set succ {a=s.b; b=pred_b} succ_b;
        set pred {a=s.a; b=succ_a} pred_a;
        set pred {a=s.b; b=succ_b} pred_b;
    in

    let insere s sa pb =
        (* print_string "insere"; *)

        (* Insere le segment s tout en reconstruisant le réseau de segments
           A la fin de l'opération, succ[s] = sa et pred[s^-1] = pb
           voir pdf *)

        let a = s.a and b = s.b in 
        let pa = find pred (mks a sa) in
        let sb = find succ (mks b pb) in

        set succ (mks a pa) b;
        set succ (mks a b) sa;
        set pred (mks a sa) b;
        set pred (mks a b) pa;

        set succ (mks b pb) a;
        set succ (mks b a) sb;
        set pred (mks b sb) a;
        set pred (mks b a) pb;
    in

    let common_tangent x0 y0 =
        (* print_string "tangent"; *)

        (* Trouve la "bottom_tangent" de deux enveloppes convexes A et B
        en partant des points x0 ∈ A, y0 ∈ B.
        En clair, la fonction tourne efficacement autour des deux enveloppes convexes jusqu'à
        trouver les deux points qui forment la "bottom_tangent"
        voir pdf*)

        let x, y = ref x0, ref y0 in

        let z0 = ref (find first !y) in
        let z1 = ref (find first !x) in

        let z2 = ref (find pred (mks !x !z1)) in
        
        let _bool = ref true in
        while !_bool do
            if is_direct_triangle (mkt !x !y !z0) < 0. then (*Si le triangle n'est pas direct *)
                begin
                let _y = !y in
                y  := !z0;
                z0 := (find succ (mks !z0 _y));
                end
            else if is_direct_triangle (mkt !x !y !z2) < 0. then
                begin
                let _x = !x in
                x  := !z2;
                z2 := (find pred (mks !z2 _x));
                end
            else _bool := false done;
        mks !x !y in

    let merge x0 y0 =
        (* print_string "merge"; *)

        (* Fusionne deux enveloppes convexes A et B
        avec x0 ∈ A, y0 ∈ B. [x0y0] doit être la tangente commune
        Pour cela, on met à jour les informations de succ, pred et
        first. *) 

        let x = ref x0 and y = ref y0 in
        insere (mks !x !y) (find first !x) (find pred (mks !y (find first !y)));
        set first !x !y;

        let _y1 _ =
            (* print_string "y1"; *)

            if (is_direct_triangle (mkt !x !y (find pred (mks !y !x))) > 0.) then
                
                let y1 = ref (find pred (mks !y !x)) in
                let y2 = ref (find pred (mks !y !y1)) in

                while incircle !y2 (mkt !x !y !y1) do
                    delete (mks !y !y1);
                    y1 := !y2;
                    y2 := find pred (mks !y !y1);
                done;
            !y1
            else let y1 = ref Not_def in 
            !y1
            in
        
        let _x1 _ =
            (* print_string "x1"; *)

            if (is_direct_triangle (mkt !x !y (find succ (mks !x !y))) > 0.) then
                
                let x1 = ref (find succ (mks !x !y)) in
                let x2 = ref (find succ (mks !x !x1)) in

                while incircle !x2 (mkt !x !y !x1) do
                    delete (mks !x !x1);
                    x1 := !x2;
                    x2 := find succ (mks !x !x1);
                done;
            !x1
            else let x1 = ref Not_def in
            !x1
            in
        
        let _bool = ref true in
        (* print_string "while"; *)
        while (!_bool) do
            let y1 = ref (_y1 ()) in
            let x1 = ref (_x1 ()) in
            
            match !x1, !y1 with
            | Not_def, Not_def -> (_bool := false; )
            | Not_def, _ -> insere (mks !y1 !x) !y !y; y := !y1;
            | _, Not_def -> insere (mks !y !x1) !x !x; x := !x1;
            | a, b when (incircle !x1 (mkt !x !y !y1)) ->
            insere (mks !y !x1) !x !x; x := !x1;
            |_ -> insere (mks !y1 !x) !y !y; y := !y1;
            done;

        set first !y !x; in
        
    let rec compute _points =
        (* print_string "compute"; *)

        (* """Calcule la triangulation de Delaunay et l'enveloppe convexe d'une
        liste de _points en mettant à jour les structures de données
        succ, pred et first.

        Préconditions :
        -la liste _points contient au moins deux éléments
        -les _points sont tous distincts""" *)

        match _points with
        | [|a; b|] ->
            (* print_string "2"; *)

            set succ (mks a b) b ; set pred (mks a b) b;
            set succ (mks b a) a ; set pred (mks b a) a;
            set first a b ; set first b a;

        | [|a; b; c|] when is_direct_triangle (mkt a b c) > 0. -> (* orientation directe *)
            (* print_string "3 direct"; *)
            (set succ (mks a c) b ; set succ (mks c a) b ; set pred (mks a c) b; set pred (mks c a) b;
            set succ (mks a b) c ; set succ (mks b a) c ; set pred (mks a b) c; set pred (mks b a) c;
            set succ (mks b c) a ; set succ (mks c b) a ; set pred (mks b c) a; set pred (mks c b) a;
            set first a b ; set first b c ; set first c a;)

        | [|a; b; c|] when is_direct_triangle (mkt a b c) < 0. -> (* orientation indirecte *)
            (* print_string "3 indirect"; *)
            (set succ (mks a b) c ; set succ (mks b a) c ; set pred (mks a b) c; set pred (mks b a) c;
            set succ (mks a c) b ; set succ (mks c a) b ; set pred (mks a c) b; set pred (mks c a) b;
            set succ (mks b c) a ; set succ (mks c b) a ; set pred (mks b c) a; set pred (mks c b) a;
            set first a c ; set first b a ; set first c b;)
                
        | [|_; _; _|] -> (* orientation alignée *)
            ((* print_string "3 align"; *)
            let t = Array.copy _points in
            Array.sort (fun a b -> compare_point a b "none") t;
            match t with  [|a; b; c|] ->
            (set succ (mks a b) b ; set pred (mks a b) b ; set succ (mks c b) b ; set pred (mks c b) b;
            set succ (mks b a) c ; set pred (mks b a) c;
            set succ (mks b c) a ; set pred (mks b c) a;
            set first a b ; set first c b;) |_ -> ())
                
        | _points -> (*dispertion horizontale *)
            (* print_string "pts"; *)
            let key = if horizontal_spreading _points then "none" else "inv" in
                let med = mediane _points key in
                let left_down, right_up = array_filter (fun x -> compare_point med x key = 1) _points in
                compute left_down;
                compute right_up;
                let s = common_tangent (array_max (fun a b -> compare_point a b key) left_down)
                                       (array_min (fun a b -> compare_point a b key) right_up) in
                merge (s.a) (s.b);
        
        
        in

    compute points; succ
    ;;


let to_list n _x _y=
    let l = ref [] in
    let append x _l = _l := x::(!_l) in
    let dict = (main (mk_array_of_points n _x _y)) in
    Hashtbl.iter (fun s p -> append ((x s.a), (y s.a), (x s.b), (y s.b)) l;
                             append ((x s.a), (y s.a), (x p), (y p)) l) dict;
    !l ;;

let to_python_list n _x _y=
    let str = ref "[" in
    let append x _s = _s := (!_s)^x in
    let dict = (main (mk_array_of_points n _x _y)) in
    Hashtbl.iter (fun s p -> append ("("^string_of_float(x s.a)^","^string_of_float(y s.a)^","^string_of_float(x s.b)^","^string_of_float(y s.b)^"),") str;
                             append ("("^string_of_float(x s.a)^","^string_of_float(y s.a)^","^string_of_float(x s.b)^","^string_of_float(y s.b)^"),") str;) dict;
    print_string ((!str)^"]") ;;


(* Communication avec le c *)

let init = Stack.create () ;;
let res = Stack.create () ;;
let size = ref 0;;

let input_c i =
    Stack.push i init;;

let delaunay_c _ =
    let points = Array.make ((Stack.length init)/2) Not_def in
    let i = ref 0 in
    while not (Stack.is_empty init) do
        let b = Stack.pop init in
        let a = Stack.pop init in
        points.(!i) <- mkp a b;
        incr i; done;
    let dict = main points in
    let rec stack_res l = match l with
        | [] -> ()
        | x::s -> let () = Stack.push x res in stack_res s in
    Hashtbl.iter (fun s p -> stack_res [(y p); (x p); (y s.a); (x s.a);
                                        (y s.b); (x s.b); (y s.a); (x s.a)]) dict;
    size := Stack.length res;;
                                

 
let size_out_c _ = !size;;

let output_c _ =
    Stack.pop res;;

let _ = Callback.register "input" input_c ;;
let _ = Callback.register "delaunay" delaunay_c ;;
let _ = Callback.register "size_out" size_out_c ;;
let _ = Callback.register "output" output_c ;;