#include <stdio.h>
#include <string.h>
#include "caml/alloc.h"
#include <caml/mlvalues.h>
#include <caml/callback.h>

// Fichier calqué sur un tutoriel

void input(double n)
{
  static const value * input_closure = NULL;
  if (input_closure == NULL) input_closure = caml_named_value("input");
  (caml_callback(*input_closure, caml_copy_double(n)));
};

void delaunay(int n)
{
  static const value * delaunay_closure = NULL;
  if (delaunay_closure == NULL) delaunay_closure = caml_named_value("delaunay");
  (caml_callback(*delaunay_closure, Val_int(n)));
};

double size_out(int n)
{
  static const value * size_out_closure = NULL;
  if (size_out_closure == NULL) size_out_closure = caml_named_value("size_out");
  return Int_val(caml_callback(*size_out_closure, Val_int(n)));
};

double output(int n)
{
  static const value * output_closure = NULL;
  if (output_closure == NULL) output_closure = caml_named_value("output");
  return Double_val(caml_callback(*output_closure, Val_int(n)));
};



