#include <stdio.h>
#include <stdlib.h>
#include <caml/callback.h>

extern void input(double n);
extern void delaunay(int n);
extern int size_out(int n);
extern double output(int n);

int length = 0;

void iter(double t[], int size)
{   int i;
    for(i=0; i<size; i++){
        input(t[i]);
    }
};

void print_array(double t[], int size)
{   int i;
    for(i=0; i<size; i++){
        printf(" %f |", t[i]);
    }
};

void init(double t[], int size)
{   int i;
    for(i=0; i<size; i++){
        t[i] = output(0);
    }
};

extern "C" double* delaunay_triangulation(double points[], int size_in){
    iter(points, size_in);
    delaunay(0);
    length = size_out(0);
    double* triang = new double[length];
    // caml_shutdown();
    init(triang, length);
    
    return triang;
};