Prérequis : ocaml, python3, g++ sur un sytème unix

delaunay.ml (ocaml) -> coeur de l'algorithme, peut fonctionner indépendemment et être interpreté
modwrapp.cc (c++) -> portage des fonctions ocaml en fonction c++
main.cc (c++) -> création d'une librairie de fonctions pour python

build.sh -> compilation (lancer "sh ./build.sh")

main.py -> fichier principal : appel les différents fichiers/fonctions